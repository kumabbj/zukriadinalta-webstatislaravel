<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan HTML</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/name" method="post">
        @csrf
        <label>First Name:</label> <br> <br>
        <input type="text" name="nama_depan"> <br> <br>
        <label>Last Name:</label> <br> <br>
        <input type="text" name="name_belakang"> <br> <br>
        <label>Gender:</label> <br> <br>
        <input type="radio" name="male">Male <br>
        <input type="radio" name="female">Female  <br>
        <input type="radio" name="other">Other <br> <br>
        <label>Nationality:</label> <br> <br>
        <select name="nation" id="nation"> 
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Thailand">Thailand</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox" name="indo">Bahasa Indonesia <br>
        <input type="checkbox" name="eng">English <br>
        <input type="checkbox" name="other">Other <br> <br>
        <label>Bio:</label> <br> <br>
        <textarea name="Bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>