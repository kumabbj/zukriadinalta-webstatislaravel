<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register' );
Route::post('/name', 'AuthController@name_post');
// Route::get('/test/{angka}', function ($angka) {
//     return view('test', ["angka" => $angka]);
// });

// Route::get('/foo', function () {
//     return 'Hello World';
// });

// Route::get('user/{id}', function ($id) {
//     return 'User '.$id;
// });